-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2021 at 03:28 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foodu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `adminID` char(5) NOT NULL,
  `adminName` varchar(200) NOT NULL,
  `adminGender` char(1) NOT NULL,
  `adminPhoneNo` varchar(20) NOT NULL,
  `username` varchar(12) NOT NULL,
  `adminIcNo` char(12) NOT NULL,
  `adminEmail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `buildingID` char(5) NOT NULL,
  `buildingName` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`buildingID`, `buildingName`) VALUES
('BD001', 'Mat Kilau'),
('BD002', 'Tun Teja 1'),
('BD003', 'Tun Teja 2');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `detailID` char(5) NOT NULL,
  `quantity` varchar(2) NOT NULL,
  `orderID` char(5) NOT NULL,
  `prodID` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderID` char(5) NOT NULL,
  `orderDate` date NOT NULL,
  `deliveryDate` date NOT NULL,
  `dormLevel` varchar(2) NOT NULL,
  `dormNo` varchar(2) NOT NULL,
  `buildingID` char(5) NOT NULL,
  `studID` int(11) NOT NULL,
  `adminID` char(5) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `prodtypes`
--

CREATE TABLE `prodtypes` (
  `typeID` char(5) NOT NULL,
  `typeName` varchar(45) NOT NULL,
  `typeDesc` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodtypes`
--

INSERT INTO `prodtypes` (`typeID`, `typeName`, `typeDesc`) VALUES
('PT001', 'Instants', 'Convenience foods which require minimal prep'),
('PT002', 'Sweets', 'Sugar-based products'),
('PT003', 'Biscuits', 'Flour-based baked food products');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prodID` char(5) NOT NULL,
  `prodName` varchar(45) NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `prodDesc` varchar(45) NOT NULL,
  `typeID` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prodID`, `prodName`, `price`, `prodDesc`, `typeID`) VALUES
('PD061', 'Oat Cookies', '1.50', 'Cookies with healthy oats and chocolate chips', 'PT003'),
('PD140', 'Maggi Kari (5px)', '3.50', 'Spicy instant noodle with kari flavour.', 'PT001'),
('PD154', 'Sugus', '1.10', 'Chewy candy with strawberry flavour.', 'PT002'),
('PD172', 'Chipsmore', '3.30', 'Cookies with chocolate chips.', 'PT003'),
('PD741', 'Bubur Knorr Cup', '2.90', 'Instant porridge with chicken flavour.', 'PT001'),
('PD752', 'Cream Crackers', '5.90', 'Wheat and soya crackers.', 'PT003');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `studID` int(11) NOT NULL,
  `studName` varchar(200) NOT NULL,
  `studGender` char(1) NOT NULL,
  `studPhoneNo` varchar(20) DEFAULT NULL,
  `MatricNo` char(10) NOT NULL,
  `studIcNo` char(12) NOT NULL,
  `studEmail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`adminID`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`buildingID`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`detailID`),
  ADD KEY `orderID` (`orderID`),
  ADD KEY `prodID` (`prodID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderID`),
  ADD KEY `buildingID` (`buildingID`),
  ADD KEY `studID` (`studID`),
  ADD KEY `adminID` (`adminID`);

--
-- Indexes for table `prodtypes`
--
ALTER TABLE `prodtypes`
  ADD PRIMARY KEY (`typeID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prodID`),
  ADD KEY `typeID` (`typeID`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`studID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `studID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderID` FOREIGN KEY (`orderID`) REFERENCES `orders` (`orderID`),
  ADD CONSTRAINT `prodID` FOREIGN KEY (`prodID`) REFERENCES `products` (`prodID`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `adminID` FOREIGN KEY (`adminID`) REFERENCES `admins` (`adminID`),
  ADD CONSTRAINT `buildingID` FOREIGN KEY (`buildingID`) REFERENCES `buildings` (`buildingID`),
  ADD CONSTRAINT `studID` FOREIGN KEY (`studID`) REFERENCES `students` (`studID`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `typeID` FOREIGN KEY (`typeID`) REFERENCES `prodtypes` (`typeID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
