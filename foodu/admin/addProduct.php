<?php

session_start();
include("connection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/admin/index.php");
}

?>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add Product</title>

<!-- Font Awesome Icon-->
<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<style>
body {font-family: "Lato", sans-serif; background: white;}

/* sidenav */
.sidenav {
	height: 100%;
	width: 250px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

img,h2,.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 19px;
	color: #D6D6D6;
	display: block;
	transition: 0.3s;
}

.sidenav #menu {
	border-bottom: 1px solid #333333;
	padding-top: 13px;
	padding-bottom: 13px;
}

.sidenav a:hover {color: #f09732;}
.sidenav a.active {color: #f09732;}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 26px;
	margin-left: 50px;
}
/* end of sidenav */

#header {
	transition: margin-left .5s;
	padding: 16px;
	background-color:#111;
	margin-left: 250px;
	color: #D6D6D6;
}

#body {
	transition: margin-left .5s;
	padding: 0;
	background-color:white;
	margin-left: 250px;
	color: #818181;
}

@media screen and (max-height: 450px) {
	.sidenav {padding-top: 15px;}
	.sidenav a {font-size: 18px;}
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	transition: 0.3s;
	border-radius: 5px;
	background:#eb785b;
	color:black;
	width:100%;
}
.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

img {border-radius: 5px 5px 0 0;}

.container {padding: 0px 16px;}

.border {
	width: 70%;
	background-color: #e6e7eb;
	padding: 11px 29px;
	border-radius: 4px;
	border: none;
	text-align: left;
	display: inline-block;
	font-size: 16px;
}
	
#user {margin-top: 0%;}

#add, #choose {
	background-color: #4475fc;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 14px;
}
#add:hover{background-color: #365cc7; cursor: pointer;}
#choose:hover{background-color: #365cc7; cursor: pointer;}

#Cancel {
	background-color: #f7462f;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 17px;
}
#Cancel:hover{background-color: #cc3d33; cursor: pointer;}

#hidden {
	visibility: hidden;
}

#visible {
	visibility: visible;
}
</style>
	
</head>
<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/admin/foodU-logo.png" style="width:50%">
	<h2>List of Menus</h2>
  <a id="menu" href="/foodu/admin/dashboard.php"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
  <a id="menu" href="/foodu/admin/adminList.php"><i class="nav-icon fas fa-users"></i> Admins</a>
  <a id="menu" href="/foodu/admin/productList.php"><i class="nav-icon fas fa-edit"></i> Products</a>
  <a id="menu" href="/foodu/admin/addProduct.php" class="active"><i class="nav-icon fas fa-plus-square"></i> Add Product</a>
  <a id="menu" href="/foodu/admin/ordersList.php"><i class="nav-icon fas fa-list-alt"></i> Orders</a>
  <a id="menu" href="/foodu/admin/plogout.php"><i class="nav-icon fas fa-sign-out-alt"></i> Log out</a>

</div>

<div id="header">
   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['adminName'];?></span>
</div>

<!-- CONTENT -->
<div id="body">
	<form id="form" action="/foodu/admin/add.php" method="POST">
	
	<h1 style="color:black;">Add Product</h1>
	<div class="card">
        <h3 style="text-align:center;"><br>Fill in details of the product</h3>
		<div class="container" style="background:white;">
		<table style="text-align:left;" cellpadding="10">
			<tr>
                <td>
					<span class="border" id="lblType">Type & Description </span>
				</td>
				<td>
					<select name="typeID" id="typeID" style="font-size:17px;">
                        <?php 
                            $sqlprodtypes = "SELECT * FROM prodtypes ORDER BY typeID ASC";
                            $qryprodtypes = mysqli_query($conn, $sqlprodtypes);
                            $rowprodtypes= mysqli_num_rows($qryprodtypes);
							

                            if($rowprodtypes > 0)
                            {
                                while($dprodtypes = mysqli_fetch_assoc($qryprodtypes))
                                {
                                    if($typeID == $dprodtypes['typeID'])
									{
                                        echo "<option value='".$dprodtypes['typeID']."'selected>".$dprodtypes['typeName'].$dprodtypes['typeDesc']."</option>";
									}	
                                    else
                                        echo "<option value='".$dprodtypes['typeID']."'>".$dprodtypes['typeName']." (".$dprodtypes['typeDesc'].")"."</option>";
								}
                            }
								
                        ?>
				    </select><br>
					<input type="text" id="hidden" style="font-size:17px;" readonly>
					 <span id="hidden"> PT001 - Instants <strong>|</strong> PT002 - Sweets <strong>|</strong> PT003 - Biscuits</span>
				</td>
				</tr><br>
					<script type="text/JavaScript">
						var select = document.getElementById('typeID');
						var input = document.getElementById('hidden');
					
						input.value = select.value;
						select.onchange = function()
						{
							input.value = select.value;
						}
						
					</script>
				  
					<tr id="chooseType"> <!-- row ni akan hilang lepas click button -->
					<td></td>
					<td><button id="choose" onclick="return show()">Choose type</button></td>
					</tr>
					<script type="text/JavaScript">
						function show() 
						{
							document.getElementById('chooseType').style.display = 'none';
							document.getElementById('typeID').style.visibility = 'hidden';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';
							document.getElementById('hidden').id = 'visible';

							var lbl = document.getElementById('lblType');
							lbl.innerHTML = "Product Type ID";
							
							return false;
						}
					</script>
			</tr>
			
			<tr id="hidden">
                <td>
					<span class="border">Product ID  </span>
				</td>
                <td>
					<input type="text" id="prodID" name="prodID" placeholder="eg: PD001" maxlength="5" style="font-size:17px;" readonly value=
					<?php
						$i = 1;
						while($i == 1)
						{
							$uniqId = substr(str_shuffle("0123456789"), 0, 3);

							$prodID = "PD".$uniqId;
							
							$sql = "SELECT prodID FROM products WHERE prodID='".$prodID."'";
							$qry=mysqli_query($conn,$sql);
							$row=mysqli_num_rows($qry);
								
							if($row > 0)
							{
								$i = 1;
							}
							else
							{
								$i = -1;
								echo $prodID;
							}
						}
					?>>
				</td>
			</tr>
			
			<tr id="hidden">
				<td>
					<span class="border">Product Name  </span>
				</td>
                <td>
					<input type="text" id="prodName" name="prodName" placeholder="eg: Butter cookies" maxlength="45" style="font-size:17px;" >
				</td>
			</tr>
			
			<tr id="hidden">
                <td>
					<span class="border">Product Description  </span>
				</td>
                <td>
					<textarea id="prodDesc" name="prodDesc" placeholder="eg: Butter cookies filled with chocolate" maxlength="45" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;"></textarea>
				</td>
			</tr>
			
			<tr id="hidden">
				<script type="text/javascript">
					function onInput(event) 
					{
						let value = parseFloat(event.value);
						if (Number.isNaN(value)) 
						{
							document.getElementById('input-1').value = "0.00";
						} 
						else 
						{
							document.getElementById('input-1').value = value.toFixed(2);
						}              
					}	
			    </script>
                <td>
					<span class="border">Price in (RM) </span>
				</td>
                <td>
					<input type="number" id="input-1" name="price" placeholder="0.00" step="0.10" oninput="onInput(this)" value="1.00" min="1.00" max="99.99" style="font-size:17px;">
					 <span style="color:red;"><strong style="color:red;">Choose the right price!</strong> The price cannot be changed after this.</span>
				</td>
			</tr>
			
			<tr id="hidden">
				<td></td>
				<td><button type="submit" id="add" name="Add" title="Button to add product into database">Add product</button>
				<button type="submit" id="Cancel" name="Cancel" onclick="return true" style="margin-left:10px;" title="Button to go back to choose product type">Cancel</button></a></td>
			</tr>
        </div>
		</table>
	</form>
	</div>
</div><br>

<script>
	//jQuery for validate blank input
	$(document).ready(function()
	{
		$('#add').click(function()
		{
			var n = $("#prodName").val();
			
			if(n =='')
			{
				alert("Please fill Product Name fields!");
				$('#prodName').css("background-color","#ffb3b3");
				return false;
			}
			else
			{
				return confirm('Are you sure you want to add this product?')
			}
		});
		
		$('input[type="text"]').focusout(function(){
			$('input[type="text"]').css("background-color", "white");
		});
	});
</script>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 