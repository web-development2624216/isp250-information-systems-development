<?php

session_start();
include("connection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/admin/index.php");
}

?>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Products</title>

<!-- Font Awesome Icon-->
<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<style>
body {font-family: "Lato", sans-serif; background: white;}

/* sidenav */
.sidenav {
	height: 100%;
	width: 250px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

img,h2,.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 19px;
	color: #D6D6D6;
	display: block;
	transition: 0.3s;
}

.sidenav #menu {
	border-bottom: 1px solid #333333;
	padding-top: 13px;
	padding-bottom: 13px;
}

.sidenav a:hover {color: #f09732;}
.sidenav a.active {color: #f09732;}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 26px;
	margin-left: 50px;
}
/* end of sidenav */

#header {
	transition: margin-left .5s;
	padding: 16px;
	background-color:#111;
	margin-left: 250px;
	color: #D6D6D6;
}

#body {
	transition: margin-left .5s;
	padding: 0;
	background-color:white;
	margin-left: 250px;
	color: #818181;
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	transition: 0.3s;
	border-radius: 5px;
	background:#eb785b;
	color:black;
	width:100%;
}
.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

img {border-radius: 5px 5px 0 0;}

.container {padding: 7px 16px; background:white;}
	
#user {margin-top: 0%;}

table
{
	border-collapse: separate; border-spacing: 0; text-align: center;
	width:100%;
	border: 1px solid #cccccc;
	/*border-radius: 5px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;*/
}
	
tr:nth-child(even) { background-color: #f2f2f2;}

th
{
	background-color: #2c3338; color: white;
	font-family: 'Maven Pro', sans-serif; 
	padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
}
tr:hover {background-color:#e6e6e6;}

td
{
	font-family: 'Roboto Condensed', sans-serif;
	border-top: 1px solid #cccccc;
	padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
}
		
#del 
{
	text-decoration: none;
	background-color: #f44336;
	border-radius: 4px;
	border: none;
	color: white;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	margin: 4px 2px;
	cursor: pointer;
	padding: 5px 10px;
}
#del:hover {background-color: #cc3d33;}
		
#edit 
{
	text-decoration: none;
	background-color: #008CBA;
	border-radius: 4px;
	border: none;
	color: white;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	margin: 4px 2px;
	cursor: pointer;
	padding: 5px 10px;
}
#edit:hover {background-color: #257793;}
</style>
</head>

<body>

<!-- SIDE NAGIVATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/admin/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/admin/dashboard.php"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
  <a id="menu" href="/foodu/admin/adminList.php"><i class="nav-icon fas fa-users"></i> Admins</a>
  <a id="menu" href="/foodu/admin/productList.php" class="active"><i class="nav-icon fas fa-edit"></i> Products</a>
  <a id="menu" href="/foodu/admin/addProduct.php" ><i class="nav-icon fas fa-plus-square"></i> Add Product</a>
  <a id="menu" href="/foodu/admin/ordersList.php"><i class="nav-icon fas fa-list-alt"></i> Orders</a>
  <a id="menu" href="/foodu/admin/plogout.php"><i class="nav-icon fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['adminName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Products</h1>
	<div class="card">
        <h3 style="text-align:center;"><br>List of product(s) available in FoodU</h3>
		<div class="container">
		<br>
		<center>
		<table>
			<tr>
				<th>ID</th>
				<th>Product Name</th>
				<th>Price (RM)</th>
				<th>Description</th>
				<th>Type</th>
				<th></th>
			</tr>
	
			<?php
			
				$sql = "SELECT * FROM products p INNER JOIN prodtypes t ON p.typeID = t.typeID ORDER BY p.prodID";
				
				$qry = mysqli_query($conn, $sql);
				$row = mysqli_num_rows($qry);

				if($row > 0)
				{
					while($r = mysqli_fetch_array($qry)) 
					{?>
						<tr>
							<td><?php echo $r['prodID']; ?></td>
							<td><?php echo $r['prodName']; ?></td>
							<td><?php echo $r['price']; ?></td>
							<td><?php echo $r['prodDesc']; ?></td>
							<td><?php echo $r['typeName']; ?></td>
							<td>
							<a id="edit" href="/foodu/admin/editProd.php?prodID=<?php echo $r['prodID']; ?>">
							<i class="fas fa-edit"></i> Edit</a>
							<a id="del" onclick="return confirm('Delete this product?');"
							href="/foodu/admin/delete.php?prodID=<?php echo $r['prodID']; ?>">
							<i class="fas fa-trash"></i> Delete</a>
							</td>
						</tr>
									
					<?php
					}
					?>
					
					<tr>
						<td colspan="6" style="text-align: left;">Total Product(s) : <?php echo $row ?></td>
					</tr>
				<?php
				}
				else
				{?>
					<tr>
						<td colspan="6" style="text-align: left;">Total Product(s) : 0</td>
					</tr>
				<?php
				}
			?>
		</table>
		</center>
		<br>
		</div>
	</div>
</div><br>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 