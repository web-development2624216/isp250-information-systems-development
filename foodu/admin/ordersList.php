<?php

session_start();
include("connection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/admin/index.php");
}

?>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Orders</title>

<!-- Font Awesome Icon-->
<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<!-- Bootstrap 4 -->
<script src="/foodu/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/foodu/admin/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/foodu/admin/dist/js/demo.js"></script>

<style>
body {font-family: "Lato", sans-serif; background: white;}

/* sidenav */
.sidenav {
	height: 100%;
	width: 250px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

img,h2,.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 19px;
	color: #D6D6D6;
	display: block;
	transition: 0.3s;
}

.sidenav #menu {
	border-bottom: 1px solid #333333;
	padding-top: 13px;
	padding-bottom: 13px;
}

.sidenav a:hover {color: #f09732;}
.sidenav a.active {color: #f09732;}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 26px;
	margin-left: 50px;
}
/* end of sidenav */

#header {
	transition: margin-left .5s;
	padding: 16px;
	background-color:#111;
	margin-left: 250px;
	color: #D6D6D6;
}

#body {
	transition: margin-left .5s;
	padding: 0;
	background-color:white;
	margin-left: 250px;
	color: #818181;
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	transition: 0.3s;
	border-radius: 5px;
	background:#eb785b;
	color:black;
	width:100%;
}
.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

img {border-radius: 5px 5px 0 0;}

.container {padding: 2px 16px; background:white;}
	
#user {margin-top: 0%;}

table
{
	border-collapse: separate; border-spacing: 0; text-align: center;
	width:100%;
	border: 1px solid #cccccc;
}

.prodStripedTR:nth-child(odd) {background-color: white;}
.prodStripedTR:nth-child(even) {background-color: #f2f2f2;}
		
th
{
	background-color: #2c3338; color: white;
	font-family: 'Maven Pro', sans-serif; 
	padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
}

td
{
	font-family: 'Roboto Condensed', sans-serif;
	padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
}

#sort {
  width: 100%;
  min-width: 15ch;
  max-width: 30ch;
  border-radius: 0.25em;
  padding: 0.25em 0.5em;
  font-size: 1.10rem;
  cursor: pointer;
  line-height: 1.1;
  background-color: #fff;
}

#Go {
	background-color: #4bad17;
	border-radius: 4px;
	border: none;
	color: white;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 1.10rem;
	margin: 4px 2px;
	cursor: pointer;
	padding: 5px 20px;
}
#Go:hover{background-color: #439c14;}

#Yes {
	background-color: #40c24b;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 5px 15px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 14px;
	width: 90px;
}

#No {
	background-color: #f44336;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 5px 15px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 14px;
	width: 90px;
	cursor: pointer;
}
#No:hover{background-color: #cc3d33;}


</style>
</head>

<body>

<!-- SIDE NAGIVATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/admin/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/admin/dashboard.php"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
  <a id="menu" href="/foodu/admin/adminList.php"><i class="nav-icon fas fa-users"></i> Admins</a>
  <a id="menu" href="/foodu/admin/productList.php"><i class="nav-icon fas fa-edit"></i> Products</a>
  <a id="menu" href="/foodu/admin/addProduct.php"><i class="nav-icon fas fa-plus-square"></i> Add Product</a>
  <a id="menu" href="/foodu/admin/ordersList.php" class="active"><i class="nav-icon fas fa-list-alt"></i> Orders</a>
  <a id="menu" href="/foodu/admin/plogout.php"><i class="nav-icon fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['adminName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Orders</h1>
	<div class="card">
        <h3 style="text-align:center;"><br>List of Order(s) in FoodU</h3>
		<div class="container">
		<br>
		<form id="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<b>Sort order by:</b>
		<select id="sort" name="value">
			<option selected="selected" name="date" value="Select Option">Select Option</option>
			<option name="date" value="All">All</option>
			<option name="date" value="Today">Today</option>
		</select>
		<input type="submit" id="Go" name="Go" value="Go"></input>
		</form>
		<table>
			<tr style="width:10%;">
				<th style="width:10%;">Order ID</th>
				<th style="width:12%">Order<br>Date</th>
				<th style="width:12%">Delivery<br>Date</th>
				<th style="width:12%">Dorm :<br>Level | No<br>Building</th>
				<th style="width:24%">Student:<br>Name</th>
				<th style="width:24%">Student:<br>Phone Number<br>Email</th>
				<th style="width:10%">Delivered</th>
			</tr>
			
			<?php
			if(isset($_POST['Go']))
			{	

				$selected=$_POST['value'];
				if($selected=='Select Option')
				{
					?>
					<script>
						alert("Please choose 'All' or 'Today'!");
					</script>
				<?php
				}
				else if($selected=='Today') //TODAY ORDER
				{
					echo '<center><b>Todays Order(s)</b></center>';
					
					$adminID = $_SESSION['adminID'];
					
					$sql = "SELECT DISTINCT * FROM orders o INNER JOIN buildings b ON o.buildingID = b.buildingID 
							INNER JOIN students s ON o.studID = s.studID INNER JOIN admins a ON o.adminID = a.adminID
							WHERE orderDate=CURRENT_DATE AND o.adminID = '$adminID'";
					
					$qry = mysqli_query($conn, $sql);
					$row = mysqli_num_rows($qry);

					if($row > 0)
					{
						echo '<marquee scrollamount="10">Click On The Order To See Order Details</marquee>';
						while($r = mysqli_fetch_array($qry)) 
						{?>
							
							<tr data-widget="expandable-table" aria-expanded="false" style="background-color: white;">
								<td><?php echo $r['orderID']; ?></td>
								<td><?php echo $r['orderDate']; ?></td>
								<td><?php echo $r['deliveryDate']; ?></td>
								<td><?php echo $r['dormLevel'], " | ", $r['dormNo'], " <br>", $r['buildingName']; ?></td>
								<td><?php echo $r['studName']; ?></td>
								<td><?php echo $r['studPhoneNo'], " <br>", $r['studEmail']; ?></td>
								<td>
								<form method="POST">
									<?php
									if ($r['status'] == 'Delivered')
									{
										echo 
										"<button id='Yes' name='Yes' disabled>". $r['status']. "</button>";
									}									
									else
									{
										echo
										"<input type='text' name='orderID' value='".$r['orderID']."' hidden='true'>
										<button id='No' name='No'>". $r['status']. "</button>";
									}
									?>
								</form>
								</td>
							</tr>
								
							<?php $id = $r['orderID'] ?>
							
							<?php
							
							$sql2 = "SELECT DISTINCT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
									INNER JOIN prodtypes pt ON p.typeID = pt.typeID WHERE orderID IN 
									(SELECT orderID FROM orders WHERE orderID = '$id')";
								
							$qry2 = mysqli_query($conn, $sql2);
							$row2 = mysqli_num_rows($qry2);
								
							if($row2 > 0)
							{?>
								<tr class="expandable-body" style="background-color: #b3b3b3;">
									<td colspan = "7">
									<center>
									<table style="border: 0px;">
									<tr style="background-color: #9ba8b0;">
										<th>#</th>
										<th>Product</th>
										<th>Type</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Total Price</th>
									</tr>
									<?php
									$i=1;
									while($r2 = mysqli_fetch_array($qry2))
									{?>
										<tr class="prodStripedTR">
											<td><?php echo $i++;?></b></td>
											<td><?php echo $r2['prodName']; ?></td>
											<td><?php echo $r2['typeName']; ?></td>
											<td><?php echo "RM ", $r2['price']; ?></td>
											<td><?php echo $r2['quantity']; ?></td>
											<td><?php echo "RM ", $r2['price'] * $r2['quantity'];?></td>
										</tr>
									<?php 
									}
									?>
									</table>
									</center>
									</td>
								</tr>
							<?php
							}
						}?>		
						<tr>
							<td colspan="7" align="left">Total Order(s) : <?php echo $row ?></td>
						</tr>
					<?php
					}
					else
					{?>
						<tr>
							<td colspan="7" align="left">Total Order(s) : 0</td>
						</tr>
					<?php
					}
					
				}
				elseif($selected=='All') //ALL ORDER
				{
				echo '<center><b>All Order(s)</b></center>';
				
				$adminID = $_SESSION['adminID'];
				
				$sql = "SELECT DISTINCT * FROM orders o INNER JOIN buildings b ON o.buildingID = b.buildingID 
						INNER JOIN students s ON o.studID = s.studID INNER JOIN admins a ON o.adminID = a.adminID 
						WHERE o.adminID = '$adminID' ORDER BY o.orderDate DESC";
				
				$qry = mysqli_query($conn, $sql);
				$row = mysqli_num_rows($qry);
					
					if($row > 0)
					{
						echo '<marquee scrollamount="10">Click On The Order To See Order Details</marquee>';
						while($r = mysqli_fetch_array($qry)) 
						{
							?>
							<tr data-widget="expandable-table" aria-expanded="false" style="background-color: white;">
								<td><?php echo $r['orderID']; ?></td>
								<td><?php echo $r['orderDate']; ?></td>
								<td><?php echo $r['deliveryDate']; ?></td>
								<td><?php echo $r['dormLevel'], " | ", $r['dormNo'], " <br>", $r['buildingName']; ?></td>
								<td><?php echo $r['studName']; ?></td>
								<td><?php echo $r['studPhoneNo'], " <br>", $r['studEmail']; ?></td>
								<td>
								<form method="POST">
									<?php
									if ($r['status'] == 'Delivered')
									{
										echo 
										"<button id='Yes' name='Yes' disabled>". $r['status']. "</button>";
									}									
									else
									{
										echo
										"<input type='text' name='orderID' value='".$r['orderID']."' hidden='true'>
										<button id='No' name='No'>". $r['status']. "</button>";
									}
									?>
								</form>
								</td>
							</tr>
							
							<?php $id = $r['orderID']; ?>
							
							<?php
							
							$sql2 = "SELECT DISTINCT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
									INNER JOIN prodtypes pt ON p.typeID = pt.typeID WHERE orderID IN 
									(SELECT orderID FROM orders WHERE orderID = '$id')";
								
							$qry2 = mysqli_query($conn, $sql2);
							$row2 = mysqli_num_rows($qry2);
								
							if($row2 > 0)
							{?>
								<tr class="expandable-body" style="background-color: #b3b3b3;">
									<td colspan = "7">
									<center>
									<table style="border: 0px;">
										<tr style="background-color: #9ba8b0;">
											<th>#</th>
											<th>Product</th>
											<th>Type</th>
											<th>Price</th>
											<th>Quantity</th>
											<th>Total Price</th>
										</tr>
									<?php
									$i=1;
									while($r2 = mysqli_fetch_array($qry2))
									{?>	
										<tr class="prodStripedTR">
											<td><?php echo $i++;?></b></td>
											<td><?php echo $r2['prodName']; ?></td>
											<td><?php echo $r2['typeName']; ?></td>
											<td><?php echo "RM ", $r2['price']; ?></td>
											<td><?php echo $r2['quantity']; ?></td>
											<td><?php echo "RM ", number_format($r2['price'] * $r2['quantity'], 2);?></td>
										</tr>									
									<?php 
									}
									?>
									</table>
									</center>
									</td>
								</tr>
							<?php
							}
						}?>	
						<tr>
							<td colspan="7" align="left">Total Order(s) : <?php echo $row ?></td>
						</tr>
					<?php
					}
					else
					{?>
						<tr>
							<td colspan="7" align="left">Total Order(s) : 0</td>
						</tr>
					<?php
					}
				}
			}

			if(isset($_POST['No'])) 
			{
				$sql = "SELECT DISTINCT * FROM orders o INNER JOIN buildings b ON o.buildingID = b.buildingID 
						INNER JOIN students s ON o.studID = s.studID INNER JOIN admins a ON o.adminID = a.adminID";
										
				$orderID=$_POST['orderID'];
				
				$update = "UPDATE orders SET status='Delivered' WHERE orderID='$orderID'";
										
				$run=mysqli_query($conn,$update);
	
				if($run)
				{
					echo
					"<script language='javascript'>
					alert('Order status has been updated to delivered.');window.location='/foodu/admin/ordersList.php';</script>";
				}
			}
			?>
		</table>
		<br>
	</div>
</div>
</div><br>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 