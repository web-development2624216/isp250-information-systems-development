<?php

session_start();
include("connection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/admin/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">
	
	<style>
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #f09732;}
	.sidenav a.active {color: #f09732;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#eb785b;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}

	.salesTable {
		border-collapse: separate; 
		border-spacing: 0; 
		text-align: center;
		width:98%;
		border: 1px solid #cccccc;
	}

	.th2{
		background-color: #2c3338; color: white;
		font-family: 'Maven Pro', sans-serif; 
		border: 1px solid white;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
	
	.tr2:nth-child(even) {background-color: #f2f2f2;}

	.td2{
		font-family: 'Roboto Condensed', sans-serif;
		border-top: 1px solid #cccccc;
		padding-top: 15px; 
		padding-right: 15px; 
		padding-bottom: 15px; 
		padding-left: 15px;
	}
	</style>
</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/admin/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/admin/dashboard.php" class="active"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
  <a id="menu" href="/foodu/admin/adminList.php"><i class="nav-icon fas fa-users"></i> Admins</a>
  <a id="menu" href="/foodu/admin/productList.php"><i class="nav-icon fas fa-edit"></i> Products</a>
  <a id="menu" href="/foodu/admin/addProduct.php" ><i class="nav-icon fas fa-plus-square"></i> Add Product</a>
  <a id="menu" href="/foodu/admin/ordersList.php"><i class="nav-icon fas fa-list-alt"></i> Orders</a>
  <a id="menu" href="/foodu/admin/plogout.php"><i class="nav-icon fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['adminName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">My Dashboard</h1>
	
	<!--Statistics -->
	<div class="card">
    <h3 style="text-align:center;"><br>Statistics</h3>
		<div class="container">
		<br>
			<table cellpadding="5" style="text-align: center">
				<tr bgcolor="#fcbc4c;">
					<td width="1%"><i class="fas fa-user">
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-receipt">
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-truck"></td>
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-plus"></td>
				</tr>
				
				<tr bgcolor="#f0f2f2">
					
					<td>
						<b>Total All Students</b><br><br>
						<?php
							$sql = "SELECT COUNT(*) as cnt FROM students";
							$result = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($result)['cnt'];
							echo $count;
						?>
					</td>
					
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Profits</b><br><br>
						<?php
						
							$adminID = $_SESSION['adminID'];
							
							$sql = "SELECT DISTINCT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
									INNER JOIN orders o ON od.orderID = o.orderID WHERE o.adminID = '$adminID' AND o.status = 'Delivered'";
									
							$qry = mysqli_query($conn, $sql);
							$row = mysqli_num_rows($qry);
							
							$total = 0;
							if($row > 0)
							{	
								while($r = mysqli_fetch_array($qry))
								{
									$total = $total + ($r['price'] * $r['quantity']);
								}
							}
							echo "RM ".$total;
						?>
					</td>
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Orders To Deliver</b><br><br>
						<?php
						
							$adminID = $_SESSION['adminID'];
						
							$sql = "SELECT COUNT(*) as cnt FROM orders o WHERE o.adminID = '$adminID' AND o.status = 'Not Delivered'";
							$result = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($result)['cnt'];
							echo $count;
						?>
					</td>
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>New Orders Today</b><br><br>
						<?php
						
							$adminID = $_SESSION['adminID'];
						
							$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate=CURRENT_DATE AND adminID = '$adminID' AND status = 'Not Delivered'";
							$result = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($result)['cnt'];
							echo $count;
						?>
					</td>
				</tr>
			</table>
		<br>
		</div>
	</div>
	
	<br><br>

	<!--Number of products sold based on types -->
	<div class="card">
	<h3 style="text-align:center;"><br>Number of Products Sold According to Product Types</h3>
		<div style="background:#66c1fa;">
			<center>
			<b>Total of All Products Sold: 
				<?php
				
					$adminID = $_SESSION['adminID'];
				
					$sql = "SELECT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
							INNER JOIN prodtypes pt ON p.typeID = pt.typeID INNER JOIN orders o ON od.orderID = o.orderID 
							WHERE o.adminID = '$adminID' AND o.status = 'Delivered'";

					$qry = mysqli_query($conn, $sql);
					$row = mysqli_num_rows($qry);
					
					$sum = 0;
					if($row > 0)
					{
						while($r = mysqli_fetch_array($qry))
						{
							$sum = $sum + $r['quantity'];
						}
					}
					echo $sum;
				?>
			</b>
			</center>
		</div>
			
		<div class="container">
			<table cellpadding="5" cellspacing="10" style="text-align: center">
				<tr>
					<td bgcolor="#fcbc4c" width="1%" align="center"><img src="/foodu/admin/instantsIcon.png" alt="Type: Instants" width="50" height="50"></img></td>
					<td bgcolor="#f0f2f2" width="10%"><b>Instants</b><br>
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						$sql = "SELECT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
								INNER JOIN prodtypes pt ON p.typeID = pt.typeID INNER JOIN orders o ON od.orderID = o.orderID 
								WHERE o.adminID = '$adminID' AND pt.typeID = 'PT001' AND o.status = 'Delivered'";

						$qry = mysqli_query($conn, $sql);
						$row = mysqli_num_rows($qry);
						
						$sum = 0;
						if($row > 0)
						{
							while($r = mysqli_fetch_array($qry))
							{
								$sum = $sum + $r['quantity'];
							}
						}
						echo $sum;
					?>
					</td>
				</tr>
					
				<tr>
					<td bgcolor="#fcbc4c" width="1%" align="center"><img src="/foodu/admin/sweetsIcon.png" alt="Type: Sweets" width="50" height="50"></img></td>
					<td bgcolor="#f0f2f2" width="10%"><b>Sweets</b><br>
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						$sql = "SELECT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
								INNER JOIN prodtypes pt ON p.typeID = pt.typeID INNER JOIN orders o ON od.orderID = o.orderID 
								WHERE o.adminID = '$adminID' AND pt.typeID = 'PT002' AND o.status = 'Delivered'";

						$qry = mysqli_query($conn, $sql);
						$row = mysqli_num_rows($qry);
						
						$sum = 0;
						if($row > 0)
						{
							while($r = mysqli_fetch_array($qry))
							{
								$sum = $sum + $r['quantity'];
							}
						}
						echo $sum;
					?>
					</td>
				</tr>
					
				<br>
					
				<tr>
					<td bgcolor="#fcbc4c" width="1%" align="center"><img src="/foodu/admin/biscuitsIcon.png" alt="Type: Biscuits" width="50" height="50"></img></td>
					<td bgcolor="#f0f2f2" width="10%"><b>Biscuits</b><br>
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						$sql = "SELECT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
								INNER JOIN prodtypes pt ON p.typeID = pt.typeID INNER JOIN orders o ON od.orderID = o.orderID 
								WHERE o.adminID = '$adminID' AND pt.typeID = 'PT003' AND o.status = 'Delivered'";

						$qry = mysqli_query($conn, $sql);
						$row = mysqli_num_rows($qry);
						
						$sum = 0;
						if($row > 0)
						{
							while($r = mysqli_fetch_array($qry))
							{
								$sum = $sum + $r['quantity'];
							}
						}
						echo $sum;
					?>
					</td>
				</tr>
			</table>
		<br>
		</div>
	</div>
	
	<br><br>

	<!--Monthly Report -->
	<div class="card">
	<h3 style="text-align:center;"><br>Sales Report In 
		<?php
			date_default_timezone_set("Asia/Kuala_Lumpur");
			echo date("F");
			echo " ";
			echo date("Y");
		?>
	</h3>
		<div class="container">
			<br>
			<table class="salesTable" align="center">
				<th class="th2" colspan="14">
					<img style="float:left;" src="/foodu/admin/upIcon.png" width="20px" height="20px">
					<span>Monthly Report</span>
					<img style="float:right;" src="/foodu/admin/downIcon.png" width="20px" height="20px"></span>
				</th>
				<tr class="tr2">
					<td class="th2">Months</td>
					<td class="th2">Number of Orders</td>
					<td class="th2">Total Sales (RM)</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>January</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (January)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-01-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (January)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-01-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>February</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (February)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-02-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (February)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-02-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>March</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (March)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-03-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (March)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-03-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>April</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (April)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-04-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (April)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-04-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>May</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (May)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-05-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (May)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-05-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>June</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (June)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-06-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (June)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-06-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>July</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (July)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-07-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (July)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-07-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>August</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (August)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-08-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (August)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-08-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>September</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (September)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-09-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (September)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-09-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>October</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (October)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-10-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (October)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-10-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>November</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (November)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-11-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (November)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-11-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
				</td>
				</tr>
				<tr class="tr2">
					<td class="td2"><b>December</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (December)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE orderDate LIKE '%-12-%' AND adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (December)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID
								WHERE orderDate LIKE '%-12-%' AND o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
				<tr style="background-color: #dde2eb;">
					<td class="td2"><b>Total</b></td>
					<td class="td2">
					<?php
					
						$adminID = $_SESSION['adminID'];
					
						//Number of orders (All)
						$sql = "SELECT COUNT(*) as cnt FROM orders WHERE adminID = '$adminID' AND status = 'Delivered'";
						$result = mysqli_query($conn, $sql);
						$count = mysqli_fetch_assoc($result)['cnt'];
						echo $count;
						
						//Total Profits (All)
						$sql2 = "SELECT DISTINCT * FROM orders o INNER JOIN orderdetails od ON o.orderID = od.orderID
								INNER JOIN products p ON p.prodID = od.prodID INNER JOIN prodtypes pt ON pt.typeID = p.typeID WHERE o.adminID = '$adminID' AND o.status = 'Delivered'";
								
						$qry2 = mysqli_query($conn, $sql2);
						$row2 = mysqli_num_rows($qry2);
									
						$totalPrice=0;
						if($row2 > 0)
						{	
							while($r2 = mysqli_fetch_array($qry2))
							{
								$totalPrice = $totalPrice + ($r2['price'] * $r2['quantity']);
							}
						}
						echo "<td class='td2'>".$totalPrice."</td>";
					?>
					</td>
				</tr>
			</table>
			<br>	
		</div>
	</div>
</div><br>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 