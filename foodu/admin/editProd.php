<?php

session_start();
include("connection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/admin/index.php");
}


$prodID = $_GET['prodID']; // get id through query string

$qry = mysqli_query($conn,"SELECT * FROM products p INNER JOIN prodtypes pt ON p.typeID = pt.typeID WHERE prodID='$prodID'"); // select query

$data = mysqli_fetch_array($qry); // fetch data

if(isset($_POST['Update'])) // when click on Update button
{
	$prodName = $_POST['prodName'];
	$price = $_POST['price'];
    $prodDesc = $_POST['prodDesc'];
	$typeID = $_POST['typeID'];
	
    $update = "UPDATE products SET prodName='$prodName', price='$price', prodDesc='$prodDesc', typeID='$typeID' WHERE prodID='$prodID'";

	$run=mysqli_query($conn,$update);
	
	if($run)
	{
		echo"<script language='javascript'>
		alert('Details of product has been updated successfully.');window.location='/foodu/admin/productList.php';</script>";
	}
	else
	{
		echo "<script language='javascript'>alert('Error! Failed to update details of product');window.location='/foodu/admin/adminList.php';</script>";
	}
}
if(isset($_POST['Cancel']))
{
	echo"<script language='javascript'>window.location='/foodu/admin/productList.php';</script>";
}
?>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Products</title>

<!-- Font Awesome Icon-->
<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<style>
body {font-family: "Lato", sans-serif; background: white;}

/* sidenav */
.sidenav {
	height: 100%;
	width: 250px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

img,h2,.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 19px;
	color: #D6D6D6;
	display: block;
	transition: 0.3s;
}

.sidenav #menu {
	border-bottom: 1px solid #333333;
	padding-top: 13px;
	padding-bottom: 13px;
}

.sidenav a:hover {color: #f09732;}
.sidenav a.active {color: #f09732;}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 26px;
	margin-left: 50px;
}
/* end of sidenav */

#header {
	transition: margin-left .5s;
	padding: 16px;
	background-color:#111;
	margin-left: 250px;
	color: #D6D6D6;
}

#body {
	transition: margin-left .5s;
	padding: 0;
	background-color:white;
	margin-left: 250px;
	color: #818181;
}

@media screen and (max-height: 450px) {
	.sidenav {padding-top: 15px;}
	.sidenav a {font-size: 18px;}
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	transition: 0.3s;
	border-radius: 5px;
	background:#eb785b;
	color:black;
	width:100%;
}
.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

img {border-radius: 5px 5px 0 0;}

.container {padding: 7px 16px;}

.border {
	width: 70%;
	background-color: #e6e7eb;
	padding: 11px 29px;
	border-radius: 4px;
	border: none;
	text-align: left;
	display: inline-block;
	font-size: 16px;
}

#user {margin-top: 0%;}

#Update {
	background-color: #4475fc;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 14px;
}
#Update:hover{background-color: #365cc7; cursor: pointer;}

#Cancel {
	background-color: #f7462f;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 17px;
}
#Cancel:hover{background-color: #cc3d33; cursor: pointer;}
</style>
</head>

<body>

<!-- SIDE NAGIVATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/admin/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/admin/dashboard.php"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
  <a id="menu" href="/foodu/admin/adminList.php"><i class="nav-icon fas fa-users"></i> Admins</a>
  <a id="menu" href="/foodu/admin/productList.php" class="active"><i class="nav-icon fas fa-edit"></i> Products</a>
  <a id="menu" href="/foodu/admin/addProduct.php" ><i class="nav-icon fas fa-plus-square"></i> Add Product</a>
  <a id="menu" href="/foodu/admin/ordersList.php"><i class="nav-icon fas fa-list-alt"></i> Orders</a>
  <a id="menu" href="/foodu/admin/plogout.php"><i class="nav-icon fas fa-sign-out-alt"></i> Log out</a>

</div>

<div id="header">
   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float: right"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['adminName'];?></span>
</div>

<!-- CONTENT -->
<div id="body">

	<form method="POST">
	
	<h1 style="color:black;">Edit Product</h1>
	<div class="card">
        <h3 style="text-align:center;"><br>Edit the details of the selected product</h3>
		<div class="container" style="background:white;">
		<table style="text-align:left;" cellpadding="10">
			<tr>
                <td>
					<span class="border">Type & Description </span>
				</td>
				<td>
					<select name="typeID" id="typeID" style="font-size:17px;">
					
						<?php 
                            $sqlprodtypes = "SELECT * FROM prodtypes ORDER BY typeID ASC";
                            $qryprodtypes = mysqli_query($conn, $sqlprodtypes);
                            $rowprodtypes= mysqli_num_rows($qryprodtypes);
					
							if($rowprodtypes > 0)
                            {
                                while($dprodtypes = mysqli_fetch_assoc($qryprodtypes))
                                {
                                    if($data['typeID'] == $dprodtypes['typeID'])
                                        echo "<option value='".$dprodtypes['typeID']."'selected>".$dprodtypes['typeName']." (".$dprodtypes['typeDesc'].")"."</option>";
                                    else
                                        echo "<option value='".$dprodtypes['typeID']."'>".$dprodtypes['typeName']." (".$dprodtypes['typeDesc'].")"."</option>";
								}
                            }
						?>
					</select>
				</td>
			</tr>
			
			<tr>
                <td>
					<span class="border">Product ID  </span>
				</td>
                <td>
					<input type="hidden" name="prodID" value="<?php echo $data['prodID'] ?>" maxlength="5" style="font-size:17px;"><?php echo $data['prodID'] ?></input>
				</td>
			</tr>
			
			<tr>
				<td>
					<span class="border">Product Name  </span>
				</td>
                <td>
					<input type="text" id="name" name="prodName" value="<?php echo $data['prodName'] ?>" placeholder="<?php echo $data['prodName'] ?>" maxlength="45" style="font-size:17px;"/>
				</td>
			</tr>
			
			<tr>
                <td>
					<span class="border">Product Description  </span>
				</td>
                <td>
					<textarea name="prodDesc" id="desc" placeholder="<?php echo $data['prodDesc'] ?>" maxlength="45" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;"><?php echo $data['prodDesc'] ?></textarea>
				</td>
			</tr>
			
			<tr>
                <td>
					<span class="border">Price in (RM) </span>
				</td>
                <td>
					<input type="hidden" id="input-1" name="price" value="<?php echo $data['price'] ?>" style="font-size:17px;"><?php echo $data['price'] ?></input>
				</td>
			</tr>
			
			<tr>
				<td></td>
				<td><button type="submit" id="Update" name="Update" title="Button to update details of product">Update product</button>
				<button type="submit" id="Cancel" name="Cancel" onclick="return true" style="margin-left:10px;" title="Button to go back to list of products">Cancel</button></a></td>
				
			</tr>
        </div>
		</table>
	</form>
	</div>
</div><br>

<script>
	//jQuery for validate blank input
	$(document).ready(function()
	{
		$('#Update').click(function()
		{
			var n = $("#name").val();
			
			if(n =='')
			{
				alert("Please fill Product Name fields!");
				$('#name').css("background-color","#ffb3b3");
				return false;
			}
			else
			{
				return confirm('Are you sure you want to update this product?');
			}
		});
		
		$('input[type="text"]').focusout(function(){
			$('input[type="text"]').css("background-color", "white");
		});
	});
</script>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 