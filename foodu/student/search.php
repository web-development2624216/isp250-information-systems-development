<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Search</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
	
	<!-- Google Font -->
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>
    
	<style>
	/*RESTRICTED AREA (DO NOT CHANGE) */
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #4799eb;}
	.sidenav a.active {color: #4799eb;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#33a6cc;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}
	/*END OF RESTRICTED AREA  */
        
    /* style ddbtn */
    .search input[type=text] {
		font-size: 17px;
		border: 1px solid grey;
		width: 50%;
		background: #f1f1f1;
		height: 41px;
		padding-left:10px;
	}
		
	.search .search-container button{
		width: 10%;
		color: white;
		font-size: 17px;
		border: 1px solid grey;
		cursor: pointer;
        margin-top: 8px;
        background: #00b36b;
		height: 41px;
	}

	.search .search-container button:hover{
        background: #00995c;
    }
        
	</style>
</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2> 
  
  <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php" class="active"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<!-- RESTRICTED AREA (DO NOT CHANGE) -->
<div id="header">

   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <!-- END OF RESTRICTED AREA (DO NOT CHANGE) -->
   <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['studName'];?> </span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Search</h1>
	
	<div class="card">
    <h3 style="text-align:center;"><br>Search Product Availability</h3>
		<div class="container">
		<br>
		<b>Your Order ID :</b> <?php echo $_SESSION['orderID'];?>
		<br>
		<form action="/foodu/student/result.php" method="GET">
			<center>
			<div class="search">
				<div class="search-container">
					<input type="text" id="keyword" placeholder="All Products" name="keyword">
					<button type="submit" title="Search!"><i class="fa fa-search"></i></button>
				</div>
			</div>
			</center>
		</form>
        </div>
    </div>
</div>
<br><br>

    <script>
        <!-- RESTRICTED AREA (DO NOT CHANGE) -->
        //Script to open dan close side menu 
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("header").style.marginLeft = "250px";
            document.getElementById("body").style.marginLeft="250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("header").style.marginLeft= "0";
            document.getElementById("body").style.marginLeft="0";
        }
        <!-- END OF RESTRICTED AREA (DO NOT CHANGE) -->

        
    </script>
</div>
</body>
</html> 