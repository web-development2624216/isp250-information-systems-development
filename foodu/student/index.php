<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Student Login</title>

	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Play&display=swap">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@300&display=swap">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<style>
		body{background: #2c3338; color: white; font-family: 'Play', sans-serif;}
		
		img{width:300px;}
		
		hr{border: 1px solid white; width: 550px;}
		
		/* link to the student index.php */
		a{text-decoration: none; color: #008ae6;}	
		a:hover{color: #00bfff;}
		
		input[type=text], input[type=password] 	/* username , ic number */
		{
			width: 25%;
			margin: 10px 0;
			box-sizing: border-box;
			padding: 10px;
			font-family: 'Doppio One';
			font-size: 18px;
		}
		
		.btn	/* button Log in */
		{
			text-decoration: none;
			background-color: #0066ff;
			border-radius: 4px;
			border: none;
			color: white;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			margin: 10px 2px;
			padding: 10px 10px;
			width: 8%;
			cursor: pointer; font-family: 'Doppio One'; font-size: 15px;
		}
		.btn:hover {background-color: #0059b3;}
		
	</style>
	
	<script>
	//jQuery for validate blank input
	$(document).ready(function()
	{
		$('input[type="submit"]').click(function()
		{
			var m = $("#MatricNo").val();
			var ic = $("#studIcNo").val();
			
			// Checking for blank fields.
			if(m =='' && ic =='')
			{
				$('input[type="text"]').css("background-color","#ffb3b3");
				$('input[type="password"]').css("background-color","#ffb3b3");
				alert("Please fill all fields!");
				return false;
			}
			else if(m =='')
			{
				$('#MatricNo').css("background-color","#ffb3b3");
				alert("Please fill Matric Number fields!");
				return false;
			}
			else if(ic =='')
			{
				$('#studIcNo').css("background-color","#ffb3b3");
				alert("Please fill IC Number fields!");
				return false;
			}
			else
			{
				return true;
			}
			
		});
		
		$('#MatricNo').focus(function(){
			$('#MatricNo').css("background-color","#99ccff");
		});
		
		$('#studIcNo').focus(function(){
			$('#studIcNo').css("background-color","#99ccff");
		});
		
		$('input[type="text"]').focusout(function(){
			$('input[type="text"]').css("background-color", "white");
		});
		
		$('input[type="password"]').focusout(function(){
			$('input[type="password"]').css("background-color", "white");
		});
	});
	</script>
</head>

<body>
	<center>
		<img src="/foodu/student/foodU-logo.png" alt="FoodU Logo">
		<h1>FOOD STOCK ORDERING SYSTEM</h1>
		<hr>
		<h3 style="font-family: 'Heebo', sans-serif;">Student Login</h3>
	
		<form id="login" name="login" method="post" action="/foodu/student/plogin.php">
			<input type="text" name="MatricNo" id="MatricNo" placeholder="Matric Number" maxlength = "10" /><br>
			<input type="password" name="studIcNo" id="studIcNo" placeholder="IC Number" maxlength = "12" /><br>
			<input type="submit" class="btn" name="btnlogin" value="Log in"/>
		</form>
		
		<p style="font-family: 'Heebo', sans-serif; font-weight: bold"><a href="/foodu/admin/index.php">Administrator Login</a></p>
		<p style="font-family: 'Heebo', sans-serif; font-weight: bold"><a href="/foodu/student/membersInfo.html">Group Members' Information</a></p>
	</center>
	
</body>
	
</html>