<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}


$prodID1 = $_GET['prodID']; // get id through query string

$qry = mysqli_query($conn,"SELECT * FROM products p INNER JOIN prodtypes pt ON p.typeID = pt.typeID WHERE prodID='$prodID1'"); // select query

$data = mysqli_fetch_array($qry); // fetch data



if (isset($_POST['select']))
{
	$orderID = $_SESSION['orderID'];
	
	$detailID = $_POST['detailID'];
	$prodID = $_POST['prodID'];
	$studID = $_POST['studID'];
	$prodName = $_POST['prodName'];

	function checkOrderID($conn,$orderID)
	{
		$found = false;
		$sql = "SELECT orderID FROM orders WHERE orderID='".$orderID."'";
		$qry=mysqli_query($conn,$sql);
		$row=mysqli_num_rows($qry);
		
		if($row > 0)
		{
			$found = true;
		}
		return $found;
	}
	
	function checkDetailID($conn,$detailID,$orderID)
	{
		$found = false;
		$sql = "SELECT detailID FROM orderdetails WHERE detailID='".$detailID."' AND orderID = '".$orderID."'";
		$qry=mysqli_query($conn,$sql);
		$row=mysqli_num_rows($qry);
		
		if($row > 0)
		{
			$found = true;
		}
		return $found;
	}
	
	function checkProdID($conn,$prodID,$orderID)
	{
		$found = false;
		$sql = "SELECT prodID FROM orderdetails WHERE prodID='".$prodID."' AND orderID = '".$orderID."'";
		$qry=mysqli_query($conn,$sql);
		$row=mysqli_num_rows($qry);
		
		if($row > 0)
		{
			$found = true;
		}
		return $found;
	}


	if(checkOrderID($conn, $orderID) == false) //new order
	{
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$todayDate = date("Y-m-d"); //2020-11-03
		
		$insertOrders = "INSERT INTO orders(orderID,orderDate,deliveryDate,dormLevel,dormNo,buildingID,studID,adminID,status)
						values('$orderID','$todayDate','$todayDate','1','1','BD001', $studID,'ADM01','Not Delivered')";
		
		if (mysqli_query($conn, $insertOrders)) 
		{
			if(checkDetailID($conn, $detailID, $orderID) == false) //don't have same detailID
			{
				$insertOrderDetails = "INSERT INTO orderdetails(detailID,quantity,orderID,prodID)values('$detailID',1,'$orderID','$prodID')";
					
				if (mysqli_query($conn, $insertOrderDetails)) 
				{
					$_SESSION['logoutPermission'] = 0; //no log out
					
					echo "<script language='javascript'>
					alert('Product on new order has been add into the cart.');window.location='/foodu/student/startSearch.php';</script>";
				} 
				else 
				{
					echo "Error: " . $insertOrderDetails . "<br>" . mysqli_error($conn);
				}
			}
			else
			{
				echo "<script language='javascript'>
				alert('Detail ID has already existed.');</script>";
			}
			
		} 
		else 
		{
		  echo "Error: " . $insertOrders . "<br>" . mysqli_error($conn);
		}
	}
	else //checkDetailID($conn, $orderID) == true ; order more than 1 product OR add order details on existing orderID
	{
		if(checkDetailID($conn, $detailID, $orderID) == false) //don't have same detailID
		{
			if(checkProdID($conn, $prodID, $orderID) == false) //don't have same prodID
			{
				$insertOrderDetails = "INSERT INTO orderdetails(detailID,quantity,orderID,prodID)values('$detailID',1,'$orderID','$prodID')";
						
				if (mysqli_query($conn, $insertOrderDetails)) 
				{
					$_SESSION['logoutPermission'] = 0; //no log out
					
					echo "<script language='javascript'>
					alert('Product on current order has been add into the cart.');window.location='/foodu/student/startSearch.php';</script>";
				} 
				else 
				{
					echo "Error: " . $insertOrderDetails . "<br>" . mysqli_error($conn);
				}
			}
			else
			{
				echo "<script language='javascript'>
				alert('Product ID has already existed.');</script>";
			}
		}
		else
		{
			echo "<script language='javascript'>
			alert('Detail ID has already existed.');</script>";
		}
		
	}
}

if(isset($_POST['another']))
{
	echo"<script language='javascript'>window.location='/foodu/student/startSearch.php';</script>";
}

?>


<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Search</title>

<!-- Font Awesome Icon-->
<link rel="stylesheet" href="/foodu/admin/plugins/fontawesome-free/css/all.min.css">

<style>
body {font-family: "Lato", sans-serif; background: white;}

/* sidenav */
.sidenav {
	height: 100%;
	width: 250px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

img,h2,.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 19px;
	color: #D6D6D6;
	display: block;
	transition: 0.3s;
}

.sidenav #menu {
	border-bottom: 1px solid #333333;
	padding-top: 13px;
	padding-bottom: 13px;
}

.sidenav a:hover {color: #4799eb;}
.sidenav a.active {color: #4799eb;}

.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 26px;
	margin-left: 50px;
}
/* end of sidenav */

#header {
	transition: margin-left .5s;
	padding: 16px;
	background-color:#111;
	margin-left: 250px;
	color: #D6D6D6;
}

#body {
	transition: margin-left .5s;
	padding: 0;
	background-color:white;
	margin-left: 250px;
	color: #818181;
}

@media screen and (max-height: 450px) {
	.sidenav {padding-top: 15px;}
	.sidenav a {font-size: 18px;}
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	transition: 0.3s;
	border-radius: 5px;
	background:#33a6cc;
	color:black;
	width:100%;
}
.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

img {border-radius: 5px 5px 0 0;}

.container {padding: 7px 16px;}

.border {
	width: 70%;
	background-color: #e6e7eb;
	padding: 11px 29px;
	border-radius: 4px;
	border: none;
	text-align: left;
	display: inline-block;
	font-size: 16px;
}

#user {margin-top: 0%;}

#select {
	background-color: #4475fc;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
}
#select:hover{background-color: #365cc7; cursor: pointer;}

#another {
	background-color: #5b37c8;
	border-radius: 4px;
	border: none;
	color: white;
	padding: 10px 29px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 15px;
}
#another:hover{background-color: #492ca0; cursor: pointer;}

</style>
</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php" class="active"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<div id="header">
   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float: right"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['studName'];?></span>
</div>

<!-- CONTENT -->
<div id="body">

	<h1 style="color:black;">Search</h1>
		
	<div class="card">
        <h3 style="text-align:center;"><br>Details of the selected product</h3>
		<div class="container" style="background:white;">
	<form method="POST">
		<br>
		<b>Your Order ID :</b> <?php echo $_SESSION['orderID'];?>
		<table style="text-align:left;" cellpadding="10">
		
		<tr>
			<td>
				<input type="hidden" name="detailID" maxlength="5" value=
				<?php
					$i = 1;
					while($i == 1)
					{
						$uniqId = substr(str_shuffle("0123456789"), 0, 3);

						$detailID = "DT".$uniqId;
									
						$sql = "SELECT detailID FROM orderdetails WHERE detailID='".$detailID."'";
						$qry=mysqli_query($conn,$sql);
						$row=mysqli_num_rows($qry);
						
						if($row > 0)
						{
							$i = 1;
						}
						else
						{
							$i = -1;
							echo $detailID;
						}
					}
				?>>
				<input type="hidden" name="studID" value="<?php echo $_SESSION['studID'] ?>" readonly>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="border">Product ID  </span>
			</td>
			<td>
				<input type="hidden" name="prodID" value="<?php echo $data['prodID'] ?>" style="font-size:17px;" readonly><?php echo $data['prodID'] ?></input>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="border">Product Name  </span>
			</td>
			<td>
				<input type="hidden" name="prodName" value="<?php echo $data['prodName'] ?>" style="font-size:17px;" readonly><?php echo $data['prodName'] ?></input>
			</td>
		</tr>
					
		<tr>
			<td>
				<span class="border">Price  </span>
			</td>
			<td>
				<input type="hidden" name="price" value="<?php echo $data['price'] ?>" style="font-size:17px;" readonly><?php echo $data['price'] ?></input>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="border">Product Description  </span>
			</td>
			<td>
				<input type="hidden" name="prodDesc" value="<?php echo $data['prodDesc'] ?>" style="font-size:17px;" readonly><?php echo $data['prodDesc'] ?></input>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="border">Type Name  </span>
			</td>
			<td>
				<input type="hidden" name="typeName" value="<?php echo $data['typeName'] ?>" maxlength="5" style="font-size:17px;" readonly><?php echo $data['typeName'] ?></input>
			</td>
		</tr>
			
		<tr>
			<td></td>
			<td><button type="submit" id="select" onclick="return confirm('Are you sure you want to select this product?');" name="select" title="Select this product">Add To Cart</button>
			<button type="submit" id="another" name="another" style="margin-left:10px;" title="Button to go back to search">Select Another Product</button></td>
		</tr>
		
		</table>
		</div>
	</form>
	</div>
</div>
<br><br>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html>