<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
	
	<style>
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #4799eb;}
	.sidenav a.active {color: #4799eb;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#33a6cc;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}
	
	/*Profile*/ 
	.border {
		width: 50%;
		background-color: #e6e7eb;
		padding: 11px 29px;
		border-radius: 4px;
		border: none;
		text-align: center;
		display: inline-block;
		font-size: 16px;
	}
	
	</style>
</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>
  
  <a id="menu" href="/foodu/student/home.php" class="active"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['studName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">My Home</h1>
	
	<!--Statistics -->
	<div class="card">
    <h3 style="text-align:center;"><br> Statistics</h3>
	
		<div class="container">
		<br>
			<table cellpadding="5" style="text-align: center">
				<tr bgcolor="#fcbc4c;">
					<td width="1%"><i class="fas fa-user">
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-receipt">
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-utensils"></td>
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="1%"><i class="fas fa-truck"></td>
				</tr>
				
				<tr bgcolor="#f0f2f2">
					
					<td>
						<b>Total All Students</b><br><br>
						<?php
							$sql = "SELECT COUNT(*) as cnt FROM students";
							$result = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($result)['cnt'];
							echo $count;
						?>
					</td>
					
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Orders Made</b><br><br>
						<?php
						
							$studID = $_SESSION['studID'];
							
							$sql = "SELECT COUNT(*) as cnt FROM orders WHERE studID='$studID'";
							$result = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($result)['cnt'];
							echo $count;
						?>
					</td>
					
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Products Purchased</b><br><br>
						<?php
						
							$studID = $_SESSION['studID'];
						
							$sql = "SELECT DISTINCT COUNT(od.detailID) as cnt FROM orderdetails od 
							INNER JOIN orders o ON od.orderID = o.orderID WHERE o.studID='$studID'";

							$qry = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($qry)['cnt'];
							echo $count;
						?>
					</td>
					
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Orders to Receive</b><br><br>
						<?php
						
							$studID = $_SESSION['studID'];
						
							$sql = "SELECT DISTINCT COUNT(status) as cnt FROM orders WHERE studID='$studID' AND status='Not Delivered'";

							$qry = mysqli_query($conn, $sql);
							$count = mysqli_fetch_assoc($qry)['cnt'];
							echo $count;
						?>
					</td>
				</tr>
			</table>
		<br>
		</div>
		
	</div>
	
	<br><br>
	
	<!--Profile -->
	<div class="card">
    <h3 style="text-align:center;"><br>Profile</h3>
		<div class="container">
		<br>
			<?php
			
				$studID = $_SESSION['studID'];
			
				$sql = "SELECT * FROM students s WHERE s.studID = '$studID'";
				
				$qry = mysqli_query($conn, $sql);
				$row = mysqli_num_rows($qry);

				if($row > 0)
				{
					$r = mysqli_fetch_assoc($qry); ?>
					
					<center>
					<table style="text-align:left;" cellpadding="10">
					<tr>
						<td style="width:17%"><span class="border">ID </span></td>
						<td style="width:17%"><?php echo $r['studID']; ?></td>
						
						<td style="width:17%"><span class="border">Name </span></td>
						<td style="width:25%"><?php echo $r['studName']; ?></td>
						
						<td style="width:17%"><span class="border">Gender </span></td>
						<td style="width:17%"><?php echo $r['studGender']; ?></td>
					</tr>
						
					<tr>
						<td><span class="border">Matric Number </span></td>
						<td><?php echo $r['MatricNo']; ?></td>
						
						<td><span class="border">IC Number </span></td>
						<td><?php echo $r['studIcNo']; ?></td>
					</td>
						
					<tr>
						<td><span class="border">Phone Number </span></td>
						<td><?php echo $r['studPhoneNo']; ?></td>
						
						<td><span class="border">Email </span></td>
						<td><?php echo $r['studEmail']; ?></td>
						
					</tr>
					</table>
					</center>
				<br>
				<?php
				}
				else
				{
					echo "Error Displaying Your Profile";
				}
				
			?>
		</div>
	</div>
</div>
<br><br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 