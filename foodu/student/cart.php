<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cart, Dorm & Date</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<style>
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #4799eb;}
	.sidenav a.active {color: #4799eb;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#33a6cc;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}

	/*Cart List*/ 
	table.cart
	{
		border-collapse: separate; border-spacing: 0; text-align: center;
		width:100%;
		border: 1px solid #cccccc;
	}
		
	table.cart tr:nth-child(even) { background-color: #f2f2f2;}
		
	table.cart th
	{
		background-color: #2c3338; color: white;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
		
	table.cart td
	{
		border-top: 1px solid #cccccc;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
	
	#del 
	{
		text-decoration: none;
		background-color: #f44336;
		border-radius: 4px;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		margin: 4px 2px;
		cursor: pointer;
		padding: 5px 10px;
	}
	#del:hover {background-color: #cc3d33;}
	
	#edit 
	{
		text-decoration: none;
		background-color: #008CBA;
		border-radius: 4px;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		margin: 4px 2px;
		cursor: pointer;
		padding: 5px 10px;
	}
	#edit:hover {background-color: #257793;}
	
	.border {
		width: 50%;
		background-color: #e6e7eb;
		padding: 11px 29px;
		border-radius: 4px;
		border: none;
		text-align: center;
		display: inline-block;
		font-size: 16px;
	}
	
	#checkout {
		background-color: #4475fc;
		border-radius: 4px;
		border: none;
		color: white;
		padding: 10px 29px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 15px;
	}
	#checkout:hover{background-color: #365cc7; cursor: pointer;}

	#cancel {
		background-color: #f7462f;
		border-radius: 4px;
		border: none;
		color: white;
		padding: 10px 29px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 15px;
	}
	#cancel:hover{background-color: #cc3d33; cursor: pointer;}
</style>

</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2> 
  
  <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php" class="active"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

<h3 id="user" align="center">Welcome to FoodU!</h3>
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
<span style="float:right;"><i class="fa fa-user-circle"></i> <?php echo $_SESSION['studName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black; font-family: 'Jost', sans-serif;">Cart, Dorm & Date</h1>
	
	<!--WRITE YOUR CONTENT HERE -->
	<div class="card">
    <h3 style="text-align:center;"><br>Update the cart,<br>choose delivery date and fill in dorm details</h3>
		<div class="container">
		
		<table style="text-align:left; width: 40%;" cellpadding="10">
			<tr>
				<td style="width:10%;"><span class="border">Order ID </span></td>
				<td style="width:30%;">
				<?php 
					if ($_SESSION['orderID'] != "")
						echo $_SESSION['orderID'];
					else
						echo "No yet start the search"
				?>
				</td>
			</tr>
		</table>
		
		<table class="cart">
			<thead>
                  <tr>
					<th>Detail ID</th>
                    <th>Product Name</th>
                    <th>Type Name</th>
					<th>Price</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
					<th></th>
                  </tr>
            </thead>
			  
            <tbody>
					<?php
					
					$orderID = $_SESSION['orderID'];
					
					$sql = "SELECT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID
					INNER JOIN prodtypes t ON p.typeID = t.typeID WHERE od.orderID = '$orderID' ORDER BY od.detailID";
					
					$qry = mysqli_query($conn, $sql);
					$row = mysqli_num_rows($qry);

					if($row > 0)
					{
						
						while($r = mysqli_fetch_array($qry)) 
						{
						?>
							<tr>
								<td><?php echo $r['detailID']; ?></td>
								<td><?php echo $r['prodName']; ?></td>
								<td><?php echo $r['typeName']; ?></td>
								<td><?php echo "RM ", $r['price']; ?></td>
								<td><?php echo $r['quantity']; ?></td>
								<td><?php echo "RM ", number_format($r['price'] * $r['quantity'], 2);?></td>
								<td>
								<a id="edit" href="/foodu/student/editQCart.php?detailID=<?php echo $r['detailID']; ?>">
								<i class="fas fa-edit"></i> Edit</a>
								<a id="del" onclick="return confirm('Delete this product?');"
								href="/foodu/student/delete.php?detailID=<?php echo $r['detailID']; ?>">
								<i class="fas fa-trash"></i> Delete</a>
								</td>
							</tr>
							
						<?php
						}
						?>
						<tr>
							<td colspan="7" align="left">Total Product(s) Purchased : <?php echo $row ?></td>
						</tr>
						
					<?php
					}
					else
					{?>
						<tr>
							<td colspan="7" align="left">Total Product(s) Purchased : 0</td>
						</tr>
					<?php
					}?>
            </tbody>
        </table>
		
		<br><br>
		
		<center>
		<!-- CHOOSE DATE FORM -->
		<form action="/foodu/student/dateDorm.php" method="POST">
		
			<div class="container" style="background:white;border-collapse: separate;border-spacing: 0;width:50%;border: 1px solid #cccccc;">
			<h1 style="color:black;">Date Information</h1>
			<h3 style="text-align:center;">Choose delivery date</h3>
			<table style="text-align:left;" cellpadding="10">
				<tr>
					<td>
						<span class="border">Date Order</span>
					</td>
					<td>
						<div id = "do" style = "font-size:17px;">
							<?php 
								date_default_timezone_set("Asia/Kuala_Lumpur");
								echo date("Y/m/d"); 
							?>
						</div> 
					</td>
				</tr>

				<tr>
					<td>
						<span class="border">Date of Delivery</span>
					</td>
					<td>
						<?php
							date_default_timezone_set("Asia/Kuala_Lumpur");
							$todayDate = date("Y-m-d"); //2020-11-03
						?>
						<input type="date" id="deliveryDate" name="deliveryDate" style="font-size:17px;" value="<?php echo $todayDate;?>">
					</td>
				</tr>
				
				<tr>
					<td></td>
				</tr>
			</table>
			</div>

		<br><br>
		
		<!-- FILL IN DORM DETAILS FORM -->
			<div class="container" style="background:white;border-collapse: separate;border-spacing: 0;width:50%;border: 1px solid #cccccc;">
			<h1 style="color:black;">Dorm Information</h1>
				<h3 style="text-align:center;">Fill in your dorm detail</h3>
				<table style="text-align:left;" cellpadding="10">
					<tr>
						<td>
							<span class="border">Dorm Level</span>
						</td>
						<td>
							<input type="number" name="dormLevel" id="dormLevel" min = "1" max = "10" style="font-size:17px;" value="1" placeholder="1">
						</td>
					</tr>
				
					<tr>
						<td>
							<span class="border">Dorm Number</span>
						</td>
						<td>
							<input type = "number" name="dormNo" id="dormNo" min="1"  max = "226" style="font-size:17px;" value="1" placeholder="1">
						</td>
					</tr>
					
					<tr>
						<td>
							<span class="border">Building</span>
						</td>
						<td>
							<select name="buildingID" id="buildingID" style="font-size:17px;">
								<?php 
									$sqlbuildings = "SELECT * FROM buildings ORDER BY buildingName ASC";
									$qrybuildings = mysqli_query($conn, $sqlbuildings);
									$rowbuildings= mysqli_num_rows($qrybuildings);

									if($rowbuildings > 0)
									{
										while($dbuildings = mysqli_fetch_assoc($qrybuildings))
										{
											if($buildingID == $dbuildings['buildingID'])
												echo "<option value='".$dbuildings['buildingID']."'selected>".$dbuildings['buildingName']."</option>";
											else
												echo "<option value='".$dbuildings['buildingID']."'>".$dbuildings['buildingName']."</option>";
										}
									}
								?>
							</select>				
						</td>
					</tr>
					
					<tr>
						<td></td>
					</tr>
				</table>
			</div>
			
			<br>
			
			<?php
				if($_SESSION['logoutPermission'] == 0) //No
				{?>
					<button type='submit' id='checkout' name='checkout' title='Button to checkout order'>Checkout order</button>
					<span style='padding-left:20px;'></span>
					<button type='submit' id='cancel' name='cancel' onclick="return confirm('Cancel this order?');" title='Button to cancel order'>Cancel order</button>
				<?php
				}
			?>
			
		</form>
		</center>
		</div>
	</div>
</div>
<br><br>

<script>
	//jQuery for validate blank input
	$(document).ready(function()
	{
		$('#checkout').click(function()
		{
			var level = $("#dormLevel").val();
			var id = $("#buildingID").val();
			var no = $("#dormNo").val();
			var date = $("#deliveryDate").val();
			
			if(level == "10" && (id == "BD002" || id == "BD003"))
			{
				alert("Tun Teja building have only 9 level!");
				$('#dormLevel').css("background-color","#ffb3b3");
				return false;
			}
			else if (no == "" && level == "")
			{
				alert("Please fill all dorm fields!");
				$('#dormLevel').css("background-color","#ffb3b3");
				$('#dormNo').css("background-color","#ffb3b3");
				return false;
			}
			else if (no == "" || level == "")
			{
				if (no == "")
				{
					alert("Please fill Dorm Number fields!");
					$('#dormNo').css("background-color","#ffb3b3");
					return false;
				}
				else
				{
					alert("Please fill Dorm Level fields!");
					$('#dormLevel').css("background-color","#ffb3b3");
					return false;
				}
			}
			else if (date == "")
			{
				alert("Please choose delivery date!");
				$('#deliveryDate').css("background-color","#ffb3b3");
				return false;
			}
			else
			{
				return confirm('Checkout this order?');
			}
		});
		
		$('#dormLevel, #dormNo, #deliveryDate').focusout(function(){
			$('#dormLevel, #dormNo, #deliveryDate').css("background-color", "white");
		});
		
		$('#dormLevel, #dormNo, #deliveryDate').change(function(){
		   $('#dormLevel, #dormNo, #deliveryDate').css("background-color", "white");
		});
		
		$('#dormLevel, #dormNo, #deliveryDate').keyup(function(){
		   $('#dormLevel, #dormNo, #deliveryDate').css("background-color", "white");
		});
	});
</script>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 