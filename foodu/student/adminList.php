<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admins Contact</title>

	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
  
	<style>
		body { font-family: "Lato", sans-serif;fackground: white;}
		
		/* sidenav */
		.sidenav {
			height: 100%;
			width: 250px;
			position: fixed;
			z-index: 1;
			top: 0;
			left: 0;
			background-color: #111;
			overflow-x: hidden;
			transition: 0.5s;
			padding-top: 60px;
		}

		img,h2,.sidenav a {
			padding: 8px 8px 8px 32px;
			text-decoration: none;
			font-size: 19px;
			color: #D6D6D6;
			display: block;
			transition: 0.3s;
		}
		
		.sidenav #menu {
			border-bottom: 1px solid #333333;
			padding-top: 13px;
			padding-bottom: 13px;
		}
		
		.sidenav a:hover {color: #4799eb;}
		.sidenav a.active {color: #4799eb;}

		.sidenav .closebtn {
			position: absolute;
			top: 0;
			right: 25px;
			font-size: 26px;
			margin-left: 50px;
		}
		/* end of sidenav */

		#header {
			transition: margin-left .5s;
			padding: 16px;
			background-color:#111;
			margin-left: 250px;
			color: #D6D6D6;
		}

		#body {
			transition: margin-left .5s;
			padding: 0;
			background-color:white;
			margin-left: 250px;
			color: #818181;
		}

		.card {
			box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
			transition: 0.3s;
			border-radius: 5px;
			background:#33a6cc;
			color:black;
			width:100%;
		}
		.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

		img {border-radius: 5px 5px 0 0;}

		.container {padding: 2px 16px; background:white;}
			
		#user {margin-top: 0%;}
		
		/*List*/ 
		table
		{
			border-collapse: separate; border-spacing: 0; text-align: center;
			width:100%;
			border: 1px solid #cccccc;
		}
		
		tr:nth-child(even) { background-color: #f2f2f2;}
		
		th
		{
			background-color: #2c3338; color: white;
			padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
		}
		
		td
		{
			border-top: 1px solid #cccccc;
			padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
		}
		
	</style>
</head>

<body>

<!-- SIDE NAGIVATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2>

  <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php" class="active"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <span style="float: right"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['studName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Admins Contact</h1>
	<div class="card">
        <h3 style="text-align:center;"><br>List of Administrators Contact in FoodU</h3>
		
		<div class="container">
		<br>
		<center>
		<table>
			<tr>
				<th>Name</th>
				<th>Phone Number</th>
				<th>Email</th>
				<th>Building</th>
			</tr>
			
			<?php
			
				$sql = "SELECT * FROM admins ORDER BY adminID";
				
				$qry = mysqli_query($conn, $sql);
				$row = mysqli_num_rows($qry);

				if($row > 0)
				{
					while($r = mysqli_fetch_array($qry)) 
					{?>
						<tr>
							<td><?php echo $r['adminName']; ?></td>
							<td><?php echo $r['adminPhoneNo']; ?></td>
							<td><?php echo $r['adminEmail']; ?></td>
							<td>
								<?php
									if($r['adminID'] == "ADM01")
										echo "Tun Teja 1";
									else if($r['adminID'] == "ADM02")
										echo "Tun Teja 2";
									else
										echo "Mat Kilau";
								?>
							</td>
						</tr>
					<?php
					}
					?>
					
					<tr>
						<td colspan="4" style="text-align: left;">Total Admin(s) : <?php echo $row ?></td>
					</tr>
				<?php
				}
				else
				{?>
					<tr>
						<td colspan="4" style="text-align: left;">Total Admin(s) : 0</td>
					</tr>
				<?php
				}
			?>
		</table>
		</center>
		<br>
		</div>
		
	</div>
</div>
<br><br>

<script>
//Script to open dan close side menu 
function openNav() {
	document.getElementById("mySidenav").style.width = "250px";
	document.getElementById("header").style.marginLeft = "250px";
	document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
	document.getElementById("header").style.marginLeft= "0";
	document.getElementById("body").style.marginLeft="0";
}
</script>
	
</body>
</html>
