<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Receipt</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
	
	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>
	
	<style>
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #4799eb;}
	.sidenav a.active {color: #4799eb;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#33a6cc;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}

	/*Cart List*/ 
	table.orderDetails
	{
		border-collapse: separate; border-spacing: 0; text-align: center;
		width:100%;
		border: 1px solid #cccccc;
	}
		
	table.orderDetails tr:nth-child(even) { background-color: #f2f2f2;}
		
	table.orderDetails th
	{
		background-color: #2c3338; color: white;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
		
	table.orderDetails td
	{
		border-top: 1px solid #cccccc;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
	
	.border {
		width: 50%;
		background-color: #e6e7eb;
		padding: 11px 29px;
		border-radius: 4px;
		border: none;
		text-align: center;
		display: inline-block;
		font-size: 16px;
	}
	
	.alert {
	  padding: 10px;
	  background-color: white;
	  font-family: 'Doppio One'; font-size:17px;
	  width: 38%;
	  text-align: center;
	  border: 1px solid black;
	  position: absolute;
	  bottom: 20%;
	  right: 10%;
	}

	.closebtnReceipt {
	  margin-left: 15px;
	  color: black;
	  font-weight: bold;
	  float: right;
	  font-size: 22px;
	  line-height: 20px;
	  cursor: pointer;
	  transition: 0.3s;
	}

	.closebtnReceipt:hover {
	  color: red;
	}

</style>

</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2> 
  
  <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" class="active"><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<div id="header">

<h3 id="user" align="center">Welcome to FoodU!</h3>
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
<span style="float:right;"><i class="fa fa-user-circle"></i> <?php echo $_SESSION['studName'];?></span>

</div>


<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black; font-family: 'Jost', sans-serif;">Receipt</h1>
	
	<!--WRITE YOUR CONTENT HERE -->
	<div class="card">
    <h3 style="text-align:center;"><br>Receipt</h3>
		<div class="container">
		
		<!--Order ID & status-->
		<table style="text-align:center; width: 100%;" cellpadding="10">
			<tr>
				<!--Order ID-->
				<td style="width:20%;"><span class="border">Order ID</span><br><br><b>
				<?php 
					if ($_SESSION['order4Receipt'] != "")
						echo $_SESSION['order4Receipt'];
					else
						echo "No yet checkout the order";
				?>
				</b></td>
				<td></td>
				<!--Status-->
				<?php 
					$orderID = $_SESSION['order4Receipt'];
		
					$qry = mysqli_query($conn,"SELECT status FROM orders WHERE orderID='$orderID'"); // select query

					$row = mysqli_num_rows($qry);

					if($row > 0) //have checkout the order
					{
						$data = mysqli_fetch_assoc($qry);
						
						echo "<td style='width:20%;'><span class='border'>Status</span><br><br><b>".$data['status']."</b></td>";
					}
				?>
			</tr>
		</table>
		
		
		<!--Order Date, Delivery Date, Dorm Level, Dorm Number, Building-->
		<table style="text-align:center; width: 100%; ">
			<tr style="text-align:center; font-size:20px;">
				<td style="width: 30%;"></td>
				<td colspan="2" style="background-color: #e6e7eb; padding-top: 5px; padding-bottom: 5px;">Date & Dorm</td>
				<td style="width: 30%;"></td>
			<tr>
		</table>
		
		<table style="text-align:center; width: 100%;">
			<tr>
				<th>Order Date :</th>
				<th>Delivery Date :</th>
				<th>Dorm Level :</th>
				<th>Dorm Number :</th>
				<th>Building :</th>
			<tr>
			
			<tr>
				<?php 
					$orderID = $_SESSION['order4Receipt'];
		
					$qry = mysqli_query($conn,"SELECT o.orderDate, o.deliveryDate, o.dormLevel, o.dormNo, b.buildingName FROM orders o 
					INNER JOIN buildings b ON o.buildingID = b.buildingID WHERE o.orderID='$orderID'"); // select query

					$row = mysqli_num_rows($qry);

					if($row > 0) //have checkout the order
					{
						$data = mysqli_fetch_assoc($qry);
						
						echo "<td style='font-size:18px;'>".$data['orderDate']."</td>";
						echo "<td style='font-size:18px;'>".$data['deliveryDate']."</td>";
						
						echo "<td style='font-size:18px;'>". $data['dormLevel']."</td>";
						echo "<td style='font-size:18px;'>".$data['dormNo']."</td>";
						echo "<td style='font-size:18px;'>".$data['buildingName']."</td>";
					}
				?>
			</tr>
		</table>
		
		
		<!--Student-->
		<table style="text-align:center; width: 100%; padding-top: 30px;">
			<tr style="text-align:center; font-size:20px;">
				<td style="width: 30%;"></td>
				<td colspan="2" style="background-color: #e6e7eb; padding-top: 5px; padding-bottom: 5px;">Student</td>
				<td style="width: 30%;"></td>
			<tr>
		</table>
		
		<table style="text-align:center; width: 100%;">
			
			<tr>
				<th>Name :</th>
				<th>Phone Number :</th>
				<th>Email :</th>
			<tr>
		
			<tr>
				<?php 
					$orderID = $_SESSION['order4Receipt'];
		
					$qry = mysqli_query($conn,"SELECT s.studName, s.studPhoneNo, s.MatricNo, s.studIcNo, s.studEmail FROM orders o 
					INNER JOIN students s ON o.studID = s.studID WHERE o.orderID='$orderID'"); // select query

					$row = mysqli_num_rows($qry);

					if($row > 0) //have checkout the order
					{
						$data = mysqli_fetch_assoc($qry);
						
						echo "<td style='font-size:18px;'>".$data['studName']."</td>";
						
						echo "<td style='font-size:18px;'>".$data['studPhoneNo']."</td>";
						
						echo "<td style='font-size:18px;'>".$data['studEmail']."</td>";

					}
				?>
			</tr>
		</table>
		
		
		<!--Administrator-->
		<table style="text-align:center; width: 100%; padding-top: 30px;">
			<tr style="text-align:center; font-size:20px;">
				<td style="width: 30%;"></td>
				<td colspan="2" style="background-color: #e6e7eb; padding-top: 5px; padding-bottom: 5px;">Administrator</td>
				<td style="width: 30%;"></td>
			<tr>
		</table>
		
		<table style="text-align:center; width: 100%; padding-bottom: 30px;">
			
			<tr>
				<th>Name :</th>
				<th>Phone Number :</th>
				<th>Email :</th>
			<tr>
			
			<tr>
				<?php 
					$orderID = $_SESSION['order4Receipt'];
		
					$qry = mysqli_query($conn,"SELECT a.adminName, a.adminPhoneNo, a.adminIcNo, a.adminEmail FROM orders o 
					INNER JOIN admins a ON o.adminID = a.adminID WHERE o.orderID='$orderID'"); // select query

					$row = mysqli_num_rows($qry);

					if($row > 0) //have checkout the order
					{
						$data = mysqli_fetch_assoc($qry);
						
						echo "<td style='font-size:18px;'>".$data['adminName']."</td>";
						
						echo "<td style='font-size:18px;'>".$data['adminPhoneNo']."</td>";
						
						echo "<td style='font-size:18px;'>".$data['adminEmail']."</td>";
					}
				?>
			</tr>
			<tr>
				<th colspan="5"></th>
			<tr>
		</table>
		
		
		<table style="text-align:center; width: 100%;" class="orderDetails">
			<tr>
				<th>#</th>
				<th>Product</th>
				<th>Type</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Total Price</th>
			</tr>
			<tr>
				<?php
					$orderID = $_SESSION['order4Receipt'];
					
					$sql2 = "SELECT DISTINCT * FROM orderdetails od INNER JOIN products p ON od.prodID = p.prodID 
					INNER JOIN prodtypes pt ON p.typeID = pt.typeID WHERE od.orderID IN 
					(SELECT orderID FROM orders WHERE orderID = '$orderID')";
					
					$qry2 = mysqli_query($conn, $sql2);
					$row2 = mysqli_num_rows($qry2);
					
					if($row2 > 0)
					{
						$i=1;
						while($r2 = mysqli_fetch_array($qry2))
						{?>
							<tr>
								<td><?php echo $i++;?></b></td>
								<td><?php echo $r2['prodName']; ?></td>
								<td><?php echo $r2['typeName']; ?></td>
								<td><?php echo "RM ", $r2['price']; ?></td>
								<td><?php echo $r2['quantity']; ?></td>
								<td><?php echo "RM ", number_format($r2['price'] * $r2['quantity'], 2);?></td>
							</tr>
						<?php 
						}
					}
					?>
			</tr>
		</table>
		
		<br>
		<center><h3>Please Save The Receipt.<br>Thank You For Ordering With Us, See You Again!</h3></center>
		<br>
		</div>
	</div>
</div>
<br><br>

<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
   
</body>
</html> 