<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['userlogged']) || $_SESSION['userlogged'] != 1)
{
    header("Location: /foodu/student/index.php");
}

?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Search</title>
	
	<!-- Font Awesome Icon-->
	<link rel="stylesheet" href="/foodu/student/plugins/fontawesome-free/css/all.min.css">
	
	<style>
	/*RESTRICTED AREA (DO NOT CHANGE) */
	body {font-family: "Lato", sans-serif; background: white;}

	/* sidenav */
	.sidenav {
		height: 100%;
		width: 250px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		transition: 0.5s;
		padding-top: 60px;
	}

	.sidenav #menu {
		border-bottom: 1px solid #333333;
		padding-top: 13px;
		padding-bottom: 13px;
	}
	
	img,h2,.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 19px;
		color: #D6D6D6;
		display: block;
		transition: 0.3s;
	}

	.sidenav a:hover {color: #4799eb;}
	.sidenav a.active {color: #4799eb;}

	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 26px;
		margin-left: 50px;
	}
	/* end of sidenav */

	#header {
		transition: margin-left .5s;
		padding: 16px;
		background-color:#111;
		margin-left: 250px;
		color: #D6D6D6;
	}

	#body {
		transition: margin-left .5s;
		padding: 0;
		background-color:white;
		margin-left: 250px;
		color: #818181;
	}

	.card {
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		border-radius: 5px;
		background:#33a6cc;
		color:black;
		width:100%;
	}
	.card:hover {box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}

	img {border-radius: 5px 5px 0 0;}

	.container {padding: 2px 16px; background:white;}
		
	#user {margin-top: 0%;}
	/*END OF RESTRICTED AREA  */
        
	/*Search Option*/ 
	.border {
		width: 10%;
		background-color: #e6e7eb;
		padding: 11px 29px;
		border-radius: 4px;
		border: none;
		text-align: center;
		display: inline-block;
		font-size: 16px;
	}
	
	#back {
		background-color: #f7462f;
		border-radius: 4px;
		border: none;
		color: white;
		padding: 10px 29px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 15px;
	}
	#back:hover{background-color: #cc3d33; cursor: pointer;}
	
	/*List*/ 
	table
	{
		border-collapse: separate; border-spacing: 0; text-align: center;
		width:100%;
		border: 1px solid #cccccc;
	}
		
	tr:nth-child(even) { background-color: #f2f2f2;}
		
	th
	{
		background-color: #2c3338; color: white;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
	tr:hover {background-color:#e6e6e6;}
		
	td
	{
		border-top: 1px solid #cccccc;
		padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
	}
	
	#select 
	{
		text-decoration: none;
		background-color: #008CBA;
		border-radius: 4px;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		margin: 4px 2px;
		cursor: pointer;
		padding: 5px 10px;
	}
	#select:hover {background-color: #257793;}
	
	#kywrd:hover {background-color: white;}
	
	</style>
</head>

<body>

<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <img src="/foodu/student/foodU-logo.png" style="width:50%">
  <h2>List of Menus</h2> 
  
 <a id="menu" href="/foodu/student/home.php"><i class="fas fa-home"></i> Home</a>
  <a id="menu" href="/foodu/student/adminList.php"><i class="far fa-address-book"></i> Admins Contact</a>
  <a id="menu" href="/foodu/student/startSearch.php" class="active"><i class="fas fa-search"></i> Search</a>
  <a id="menu" href="/foodu/student/cart.php"><i class="fas fa-shopping-cart"></i> Cart, Dorm & Date</a>
  <a id="menu" href="/foodu/student/receipt.php" ><i class="fas fa-receipt"></i> Receipt</a>
  <a id="menu" href="/foodu/student/plogout.php"><i class="fas fa-sign-out-alt"></i> Log out</a>

</div>

<!-- HEADER -->
<!-- RESTRICTED AREA (DO NOT CHANGE) -->
<div id="header">

   <h3 id="user" align="center">Welcome to FoodU!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
  <!-- END OF RESTRICTED AREA (DO NOT CHANGE) -->
   <span style="float:right;"><i class="fa fa-user-circle" style="color: #ffffff;"></i> <?php echo $_SESSION['studName'];?></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black; font-family: 'Jost', sans-serif;">Search</h1>
	
	<div class="card">
    <h3 style="text-align:center;"><br>Select Product</h3>
		<div class="container"> 
		<br>
		<b>Your Order ID :</b> <?php echo $_SESSION['orderID'];?>
            <center>
			
			<center>
			<table style="width:100%; text-align:center; border:none;" >
			<tr id="kywrd">
				<td style="border-top:0px;">
					<span class="border">Keyword</span>
					<span style="padding-left:10px;">
					<?php
						$keywords = $_GET['keyword'];
									
						if ($keywords == "")
							echo "All";
						else
							echo $keywords; 
					?>
					</span>
				</td>
			</tr>
			</table>
			<br>
			<a id="back" title="Button to go back to search" href="/foodu/student/startSearch.php">Back</a><br>
			
            <!--form start-->
				
            <table>
                
              <tr>
				<th> Type Name </th>
				<th> Product Name </th>
				<th> Price (RM) </th>
				<th></th>
			</tr>
			  
              <!--container to display products & prodtypes start-->
              
              <?php
              
				$keywords = isset($_GET["keyword"]) ? $_GET["keyword"] : 0;
				
				$keywords = mysqli_real_escape_string($conn, $_GET["keyword"]);
				$sql = "SELECT DISTINCT * FROM products p INNER JOIN prodtypes t ON p.typeID = t.typeID 
				WHERE p.prodName LIKE '%$keywords%'";
				  
				$qry = mysqli_query($conn, $sql);
				$row = mysqli_num_rows($qry);
					
					
				if($row > 0)
				{
					while($r = mysqli_fetch_array($qry))
					{?>
						<tr>
							<td><?php echo $r['typeName']?></td>
							<td><?php echo $r['prodName']?></td>
							<td><?php echo $r['price']?></td>
							<td>
							<a id="select" href="/foodu/student/selectProd.php?prodID=<?php echo $r['prodID']; ?>">
							<i class="fas fa-check"></i> Select</a></td>
						</tr>
					<?php    
					}
				}
				?>
			  <br>
            </table>
            <br>
            </center>
		</div>
	</div>
	<br><br>
	
    <script>
        <!-- RESTRICTED AREA (DO NOT CHANGE) -->
        //Script to open dan close side menu 
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("header").style.marginLeft = "250px";
            document.getElementById("body").style.marginLeft="250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("header").style.marginLeft= "0";
            document.getElementById("body").style.marginLeft="0";
        }
        <!-- END OF RESTRICTED AREA (DO NOT CHANGE) -->
    </script>
</div>
</body>
</html> 