<?php

session_start();
include("studConnection.php");

if(!isset($_SESSION['logoutPermission']) || $_SESSION['logoutPermission'] == 0) //No
{
	echo"<script language='javascript'>
	alert('You must cancel or checkout your order before log out!');window.location='/foodu/student/cart.php';</script>";
}
else
{
	session_unset();
	session_destroy();
	header("Location: /foodu/student/index.php");
}
?>